package com.example.demossmsimple;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Slf4j
@Component
@RequiredArgsConstructor
class Runner implements ApplicationRunner {

    private final StateMachineFactory<OrderStates, OrderEvents> factory;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        //put orderid before run the machine
        Long orderId = 12345L;
        StateMachine<OrderStates, OrderEvents> machine = this.factory.getStateMachine(Long.toString(orderId));
        machine.getExtendedState().getVariables().putIfAbsent("orderId", orderId);

        machine.start();
        log.info("current state: " + machine.getState().getId().name());
        machine.sendEvent(OrderEvents.PAY);
        log.info("current state: " + machine.getState().getId().name());
//        machine.sendEvent(OrderEvents.FULFILL);
//        log.info("current state: "+machine.getState().getId().name()) ;
        machine.sendEvent(OrderEvents.CANCEL); //if fulfilled, cannot cancel anymore
        log.info("current state: " + machine.getState().getId().name());

        Message<OrderEvents> eventsMessage= MessageBuilder
                .withPayload(OrderEvents.FULFILL)
                .setHeader("a","b")
                .build();
        machine.sendEvent(eventsMessage);
        log.info("current state (after set header send eventsMessage): " + machine.getState().getId().name());

    }
}
//
//@Service
//class OrderService{
//    private final OrderRepository orderRepository;
//
//    OrderService(OrderRepository orderRepository) {
//        this.orderRepository = orderRepository;
//    }
//
//    Order createOrder(Date createdDate){
//        return orderRepository.save(new Order(createdDate,OrderStates.SUBMITTED));
//    }
//}
//@Data
//@Entity
//@AllArgsConstructor
//@NoArgsConstructor
//class Order{
//    @Id
//    @GeneratedValue
//    private Long id;
//    private Date dateTime;
//    private String state;
//
//    //take OrderStates as param, but still get String in the constructor
//    public Order(Date dateTime, OrderStates os) {
//        this.dateTime = dateTime;
//        //this.state = os.name(); //string representation of enum
//        this.setOrderState(os);
//    }
//    private OrderStates getOrderState() {
//        return OrderStates.valueOf(this.state);
//    }
//    private void setOrderState(OrderStates os) {
//        this.state=os.name();
//    }
//}
//
//interface OrderRepository extends JpaRepository<Order,Long> {
//
//}
@SpringBootApplication
public class DemoSsmSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSsmSimpleApplication.class, args);
    }

}

enum OrderEvents {
    PAY,
    FULFILL,
    CANCEL
}

enum OrderStates {
    SUBMITTED,
    PAID,
    FULFILLED,
    CANCELLED
}

@Slf4j
@Configuration
@EnableStateMachineFactory
class SimpleStateMachineConfig extends StateMachineConfigurerAdapter<OrderStates, OrderEvents> {


    @Override
    public void configure(StateMachineConfigurationConfigurer<OrderStates, OrderEvents> config) throws Exception {
        StateMachineListenerAdapter<OrderStates, OrderEvents> adapter = new StateMachineListenerAdapter() {
            @Override
            public void stateChanged(State from, State to) {
                log.info("state changed from: %s, to: %s", from , to );
            }
        };
        config.withConfiguration()
                .autoStartup(false)
                .listener(adapter);
    }

    @Override
    public void configure(StateMachineStateConfigurer<OrderStates, OrderEvents> states) throws Exception {
        states.withStates()
                .initial(OrderStates.SUBMITTED)
                .stateEntry(OrderStates.SUBMITTED, new Action<OrderStates, OrderEvents>() {
                    @Override
                    public void execute(StateContext<OrderStates, OrderEvents> stateContext) {
                        Long orderId = Long.class.cast(stateContext.getExtendedState().getVariables().getOrDefault("orderId", -1L));
                        log.info("order id is: " + orderId + ".");
                        log.info("entering submmited state!"); //perpetuate/propergate from one state to another
                    }
                })
                .state(OrderStates.PAID)
                .end(OrderStates.FULFILLED)  //end up with
                .end(OrderStates.CANCELLED);  //end up with
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<OrderStates, OrderEvents> transitions) throws Exception {
        //withlocal: fixed process
        //with external: varied according to events
        transitions.withExternal().source(OrderStates.SUBMITTED).target(OrderStates.PAID).event(OrderEvents.PAY)
                .and()
                .withExternal().source(OrderStates.PAID).target(OrderStates.FULFILLED).event(OrderEvents.FULFILL)
                .and()
                .withExternal().source(OrderStates.SUBMITTED).target(OrderStates.CANCELLED).event(OrderEvents.CANCEL)
                .and()
                .withExternal().source(OrderStates.PAID).target(OrderStates.CANCELLED).event(OrderEvents.CANCEL);
    }
}
